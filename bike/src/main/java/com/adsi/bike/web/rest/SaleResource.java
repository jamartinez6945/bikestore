package com.adsi.bike.web.rest;


import com.adsi.bike.domain.Sale;
import com.adsi.bike.service.ISaleService;
import com.adsi.bike.service.dto.SaleFilterDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class SaleResource {
    @Autowired
    ISaleService saleService;

    @PostMapping("/sale")
    public Sale create(@RequestBody Sale sale){
        return saleService.create(sale);
    }

    @PostMapping("/sale/multiple")
    public Sale create (@RequestBody List<Sale> sale){
        return saleService.createList(sale);
    }

    @GetMapping("/sale")
    public Iterable<Sale> read(){
        return saleService.read();
    }
    @GetMapping("/sale/{id}")
    public Optional<Sale> getById(@PathVariable Integer id){
        return saleService.getById(id);
    }
    @PutMapping("/sale/{id}")
    public Sale update(@RequestBody Sale sale){
        return saleService.update(sale);

    }
    //filter by model, documentNumber, email
    @GetMapping("/sale/filter")
    public Page<SaleFilterDTO>getFilter(@RequestParam(name = "pageSize") Integer pageSize,
                                        @RequestParam(name = "pageNumber")Integer pageNumber,
                                        @RequestParam(name = "model", required = false) String model,
                                        @RequestParam(name = "documentNumber", required = false) String documentNumber,
                                        @RequestParam(name = "email", required = false) String email){
        return saleService.getFilter(pageSize, pageNumber, model, documentNumber, email);
    }
}


