package com.adsi.bike.web.rest;

import com.adsi.bike.domain.Bike;
import com.adsi.bike.service.IBikeService;
import com.adsi.bike.service.dto.BikeDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api")
public class BikeResource {

    @Autowired
    IBikeService bikeService;

    // my api-rest create

    @PostMapping("/bike")
    public ResponseEntity create(@RequestBody BikeDTO bikeDTO) {
        return bikeService.create(bikeDTO);
    }

    //api-rest read all bikes

    @GetMapping("/bike")
    public Page<BikeDTO> read(@RequestParam(value = "pageSize")Integer pageSize,
                              @RequestParam(value = "pageNumber")Integer pageNumber) {
        return bikeService.read(pageSize, pageNumber);
    }

    //api-rest delete a bike
    @DeleteMapping("/bike/{id}")
    public void delete(@PathVariable Integer id) {
        bikeService.delete(id);
    }
    //api-rest update a bike

    @PutMapping("/bike")
    public Bike update(@RequestBody Bike bike) {
        return bikeService.update(bike);
    }

    //Get a Bike by id
    @GetMapping("/bike/{id}")
    public Optional<Bike> getById(@PathVariable Integer id){
        return bikeService.getById(id);
    }

    //bike/search?model=B124
    @GetMapping("/bike/search")
    public ResponseEntity search(@RequestParam(value = "serial", required = false) String serial,
                                 @RequestParam(value = "model",required = false) String model){
        return bikeService.search(serial,model);
    }

    //quantity bikes
    @GetMapping("/bike/count")
    public Integer quantityBikes(){
        return bikeService.quantityBikes();

    }
    //Inventory Value
    @GetMapping("/bike/inventory-value")
    public Double inventoryValue(){
        return bikeService.inventoryValue();
    }
    @GetMapping("/bike/query-model/{model}")
    public Iterable<Bike> findByModelQuery(@PathVariable String model){
        return bikeService.findByModelQuery(model);

    }
}