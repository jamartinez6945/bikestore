package com.adsi.bike.repository;

import com.adsi.bike.domain.Rols;
import org.springframework.data.repository.CrudRepository;

public interface RolRepository extends CrudRepository<Rols, Integer> {
}
