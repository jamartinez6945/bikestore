package com.adsi.bike.repository;

import com.adsi.bike.domain.DetailUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface DetailUserRepository extends JpaRepository<DetailUser, Long> {
    DetailUser findByDocumentNumber(String documentNumber);
}
