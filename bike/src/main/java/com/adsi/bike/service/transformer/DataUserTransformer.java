package com.adsi.bike.service.transformer;

import com.adsi.bike.domain.DetailUser;
import com.adsi.bike.domain.Users;
import com.adsi.bike.service.dto.DataUserDTO;

public class DataUserTransformer {

    public static DataUserDTO getDataUserDTO(DetailUser detailUser, Users users){
        if(detailUser == null){
            return null;
        }
        DataUserDTO dto = new DataUserDTO();
        dto.setDocumentNumber(detailUser.getDocumentNumber());
        dto.setFirstName(detailUser.getFirstName());
        dto.setLastName(detailUser.getLastName());
        dto.setUserName(users.getUserName());

        return dto;
    }

}
