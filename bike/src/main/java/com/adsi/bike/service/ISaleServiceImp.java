package com.adsi.bike.service;

import com.adsi.bike.domain.Client;
import com.adsi.bike.domain.Sale;
import com.adsi.bike.repository.ClientRepository;
import com.adsi.bike.repository.SaleRepository;
import com.adsi.bike.service.dto.SaleFilterDTO;
import com.adsi.bike.service.transformer.SaleTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class ISaleServiceImp implements ISaleService {
    @Autowired
    SaleRepository saleRepository;

    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    ClientRepository clientRepository;

    @Override
    public Iterable<Sale> read(){
        return saleRepository.findAll();
    }

    @Override
    public Optional<Sale> getById(Integer id){
        return saleRepository.findById(id);
    }
    @Override
    public Sale create(Sale sale){
        sale.setDateSale(LocalDateTime.now());
        Sale aSale = saleRepository.save(sale);
        //Use sendMail
        Optional<Client> client = clientRepository.findById(sale.getClient().getId());
        sendEmail(client.get().getEmail(),
                "Nueva venta",
                "Has realizado una compra" + client.get().getName());


        return aSale;
    }
    @Override
    public Sale update (Sale sale){
        return saleRepository.save(sale);
    }
    @Override
    public void delete (Integer id){
        saleRepository.deleteById(id);
    }
    private void sendEmail(String to, String subject, String text){
        SimpleMailMessage msg= new SimpleMailMessage();
        msg.setTo(to);
        msg.setSubject(subject);
        msg.setText(text);

        //Send mail
        javaMailSender.send(msg);
    }

    @Override
    public Sale createList(List<Sale> sale){
        Sale itemSale = new Sale();
        for (Sale item: sale){
            item.setDateSale(LocalDateTime.now());
            itemSale = saleRepository.save(item);
        }
        return itemSale;
    }

    @Override
    public Page<SaleFilterDTO> getFilter(Integer pageSize,
                                         Integer pageNumber,
                                         String model,
                                         String documentNumber,
                                         String email) {
        Pageable pageable = PageRequest.of(pageNumber,pageSize);
        if(model !=null && documentNumber !=null && email !=null){
            return saleRepository.findAllByBike_ModelAndClient_EmailAndClient_DocumentNumber(model, email, documentNumber, pageable)
                    .map(SaleTransformer::getSaleFilterDTOBySale);
        }else if (model !=null && documentNumber ==null && email !=null){
            return saleRepository.findAllByBike_ModelAndClient_Email(model, email, pageable)
                    .map(SaleTransformer::getSaleFilterDTOBySale);
        }else if (model !=null && documentNumber !=null && email ==null){
            return saleRepository.findAllByBike_ModelAndClient_DocumentNumber(model, documentNumber, pageable)
                    .map(SaleTransformer::getSaleFilterDTOBySale);
        }else if (model ==null && documentNumber !=null && email !=null){
            return saleRepository.findAllByClient_EmailAndClient_DocumentNumber(email, documentNumber, pageable)
                    .map(SaleTransformer::getSaleFilterDTOBySale);

        }else return null;

    }
}
