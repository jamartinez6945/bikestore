package com.adsi.bike.service.transformer;

import com.adsi.bike.domain.Sale;
import com.adsi.bike.service.dto.SaleFilterDTO;

public class SaleTransformer {

    public static SaleFilterDTO getSaleFilterDTOBySale(Sale sale){
        if(sale == null){
            return  null;
        }
        SaleFilterDTO dto = new SaleFilterDTO();
        dto.setIdSale(sale.getId());
        dto.setEmail(sale.getClient().getEmail());
        dto.setModel(sale.getBike().getModel());
        dto.setNameClient(sale.getClient().getName());
        dto.setPrice(sale.getBike().getPrice());

        return dto;
    }
}
